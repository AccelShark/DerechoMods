# DerechoMods
The AccelShark Derecho Server's modpack.

## Building
```shell
$ gradle jar
```

## Setting up MySQL
MySQL is needed for smart persistence.

### Example YAML config
```yaml
sql:
    uri: "jdbc:mysql://SERV:3306?user=USER&password=PASS"
```
- `SERV`: The server address
- `USER`: The MySQL username
- `PASS`: The MySQL password

Secure your MySQL server. Also store this in `plugins/derechomods/derecho.yml`.

### MySQL instantiation
TODO