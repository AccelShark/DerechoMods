package us.accelshark.derecho.mods;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.data.BlockData;
import org.bukkit.generator.ChunkGenerator;

public class DPlotChunkGenerator extends ChunkGenerator {
    private static final BlockData bottomSlabData = Bukkit.createBlockData("minecraft:smooth_stone_slab[type=bottom]");

    @Override
    public boolean isParallelCapable() {
        return true;
    }

    @Override
    public ChunkData generateChunkData(World world, Random random, int x, int z, ChunkGenerator.BiomeGrid biome) {
        // Make a blank chunk.
        ChunkData chunkData = createChunkData(world);
        // Set our chunkdata,
        // > 0: BARRIER
        // > 1: Bedrock
        // > 63 downto 2: Stone
        chunkData.setRegion(0, 0, 0, 16, 1, 16, Material.BARRIER);
        chunkData.setRegion(0, 1, 0, 16, 2, 16, Material.BEDROCK);
        chunkData.setRegion(0, 2, 0, 16, 63, 16, Material.STONE);
        int modX = x % 4;
        int modZ = z % 4;
        // Generate X paths per 4 chunks
        switch (modX) {
            case 0:
                chunkData.setRegion(0, 63, 0, 2, 64, 16, bottomSlabData);
                break;
            case -1:
            case 3:
                chunkData.setRegion(14, 63, 0, 16, 64, 16, bottomSlabData);
                break;
            default:
                break;
        }
        // Generate Z paths per 4 chunks
        switch (modZ) {
            case 0:
                chunkData.setRegion(0, 63, 0, 16, 64, 2, bottomSlabData);
                break;
            case -1:
            case 3:
                chunkData.setRegion(0, 63, 14, 16, 64, 16, bottomSlabData);
                break;
            default:
                break;
        }
        if (modX == 0 && modZ == 0) {
            int randomInt = Math.abs(random.nextInt() % 8);

            switch (randomInt) {
                case 0:
                    chunkData.setBlock(2, 63, 2, Material.ACACIA_SIGN);
                    break;
                case 1:
                    chunkData.setBlock(2, 63, 2, Material.BIRCH_SIGN);
                    break;
                case 2:
                    chunkData.setBlock(2, 63, 2, Material.CRIMSON_SIGN);
                    break;
                case 3:
                    chunkData.setBlock(2, 63, 2, Material.DARK_OAK_SIGN);
                    break;
                case 4:
                    chunkData.setBlock(2, 63, 2, Material.JUNGLE_SIGN);
                    break;
                case 5:
                    chunkData.setBlock(2, 63, 2, Material.OAK_SIGN);
                    break;
                case 6:
                    chunkData.setBlock(2, 63, 2, Material.SPRUCE_SIGN);
                    break;
                case 7:
                    chunkData.setBlock(2, 63, 2, Material.WARPED_SIGN);
                    break;
                default:
                    break;
            }
        }
        // Return this chunk. It should be ready now.
        return chunkData;
    }

    /**
     * Returns a plot ordinate, X or Z
     * 
     * @param xOrZ the raw X or Z from a Minecraft block position
     * @return the plot's ordinate
     */
    public static int plotifyBlockOrdinate(int xOrZ) {
        int unsignedXOrZ = Math.abs(xOrZ);
        unsignedXOrZ >>= 6;
        return unsignedXOrZ * ((xOrZ < 0) ? -1 : 1);
    }

    /**
     * Returns a plot ordinate, X or Z
     * 
     * @param xOrZ the raw X or Z from a Minecraft chunk position
     * @return the plot's ordinate
     */
    public static int plotifyChunkOrdinate(int xOrZ) {
        int unsignedXOrZ = Math.abs(xOrZ);
        unsignedXOrZ >>= 2;
        return unsignedXOrZ * ((xOrZ < 0) ? -1 : 1);
    }
}
