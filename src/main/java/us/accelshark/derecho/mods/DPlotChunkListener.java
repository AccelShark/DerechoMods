package us.accelshark.derecho.mods;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public class DPlotChunkListener implements Listener {
    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        if (event.isNewChunk()) {
            Chunk chunk = event.getChunk();
            int x = chunk.getX();
            int z = chunk.getZ();
            int modX = x % 4;
            int modZ = z % 4;
            int plotX = DPlotChunkGenerator.plotifyChunkOrdinate(x);
            int plotZ = DPlotChunkGenerator.plotifyChunkOrdinate(z);
            if(modX == 0 && modZ == 0) {
                Sign sign = (Sign)chunk.getBlock(2, 63, 2).getState();
                sign.setLine(0, Integer.toString(plotX) + "," + Integer.toString(plotZ));
                sign.setLine(2, "Owner:");
                try {
                    DPlotSqlResult sql = Derecho.getSqlConnection().getProtectionAt(plotX, plotZ);
    
                    if (!sql.isProtected) {
                        sign.setLine(3, ChatColor.translateAlternateColorCodes('&', "&2&lUNCLAIMED"));
                    } else {
                        sign.setLine(3, ChatColor.translateAlternateColorCodes('&', "&8" + sql.owner));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sign.update();
            }
        }
    }
}
