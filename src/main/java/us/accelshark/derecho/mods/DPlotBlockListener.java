package us.accelshark.derecho.mods;

import java.sql.SQLException;

import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class DPlotBlockListener implements Listener {
    /**
     * Tests for the general public's grid road protection.
     * 
     * @param block
     * @return whether the player can generally build or not
     */
    private boolean canBuildGenerally(Block block) {
        // Chunk info get
        Chunk chunk = block.getChunk();

        // Cache the chunk coords
        int chunkModX = chunk.getX();
        int chunkModZ = chunk.getZ();

        // Get the block coords and cache the X/Z vals
        int modX = block.getX() % 16;
        int modZ = block.getZ() % 16;

        // Modulo and sign (sign and modulo do not play nice)
        boolean isNegativeX = chunkModX < 0;
        boolean isNegativeZ = chunkModZ < 0;
        chunkModX %= 4;
        chunkModZ %= 4;
        boolean canBuildX = false;
        boolean canBuildZ = false;

        switch (chunkModX) {
            case 0:
                canBuildX = isNegativeX ? modX > -14 : modX > 2;
                break;

            case -1:
            case 3:
                canBuildX = isNegativeX ? modX < -3 : modX < 13;
                break;

            default:
                canBuildX = true;
                break;
        }
        switch (chunkModZ) {
            case 0:
                canBuildZ = isNegativeZ ? modZ > -14 : modZ > 2;
                break;

            case -1:
            case 3:
                canBuildZ = isNegativeZ ? modZ < -3 : modZ < 13;
                break;

            default:
                canBuildZ = true;
                break;
        }

        // GOTCHA: Barrier blocks (see DPlotChunkGenerator) can be broken.
        return canBuildX && canBuildZ && (block.getY() > 0);
    }

    /**
     * Handles block protection (block breaks)
     * 
     * @param event the event to determine
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (!canBuildGenerally(event.getBlock())) {
            event.setCancelled(true);

            TextComponent m0 = new TextComponent("[ DPlot ] ");
            m0.setBold(true);
            m0.setColor(ChatColor.YELLOW);
            TextComponent m1 = new TextComponent("You are not allowed to break blocks here.");
            m1.setBold(false);
            m1.setColor(ChatColor.GRAY);
            m0.addExtra(m1);
            event.getPlayer().spigot().sendMessage(m0);
        } else {
            Block block = event.getBlock();
            int plotX = DPlotChunkGenerator.plotifyBlockOrdinate(block.getX());
            int plotZ = DPlotChunkGenerator.plotifyBlockOrdinate(block.getZ());
            // Derecho.staticLogger()
            //         .info("break at " + Integer.toString(block.getX()) + "," + Integer.toString(block.getZ()));
            try {
                DPlotSqlResult sql = Derecho.getSqlConnection().getProtectionAt(plotX, plotZ);

                if (!sql.isProtected) {
                    event.setCancelled(true);

                    TextComponent m0 = new TextComponent("[ DPlot ] ");
                    m0.setBold(true);
                    m0.setColor(ChatColor.YELLOW);
                    TextComponent m1 = new TextComponent("You need to have this plot protected before building here.");
                    m1.setBold(false);
                    m1.setColor(ChatColor.GRAY);
                    m0.addExtra(m1);
                    event.getPlayer().spigot().sendMessage(m0);
                } else if (!sql.owner.equals(event.getPlayer().getName())) {
                    event.setCancelled(true);

                    TextComponent m0 = new TextComponent("[ DPlot ] ");
                    m0.setBold(true);
                    m0.setColor(ChatColor.YELLOW);
                    TextComponent m1 = new TextComponent(
                            "This plot is owned by another player. You are not allowed to break blocks here.");
                    m1.setBold(false);
                    m1.setColor(ChatColor.GRAY);
                    m0.addExtra(m1);
                    event.getPlayer().spigot().sendMessage(m0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles block protection (block placement)
     * 
     * @param event the event to determine
     */
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!canBuildGenerally(event.getBlock())) {
            event.setCancelled(true);

            TextComponent m0 = new TextComponent("[ DPlot ] ");
            m0.setBold(true);
            m0.setColor(ChatColor.YELLOW);
            TextComponent m1 = new TextComponent("You are not allowed to place blocks here.");
            m1.setBold(false);
            m1.setColor(ChatColor.GRAY);
            m0.addExtra(m1);
            event.getPlayer().spigot().sendMessage(m0);
        } else {
            Block block = event.getBlock();
            int plotX = DPlotChunkGenerator.plotifyBlockOrdinate(block.getX());
            int plotZ = DPlotChunkGenerator.plotifyBlockOrdinate(block.getZ());
            // Derecho.staticLogger()
            //         .info("place at " + Integer.toString(block.getX()) + "," + Integer.toString(block.getZ()));
            try {
                DPlotSqlResult sql = Derecho.getSqlConnection().getProtectionAt(plotX, plotZ);

                if (!sql.isProtected) {
                    event.setCancelled(true);

                    TextComponent m0 = new TextComponent("[ DPlot ] ");
                    m0.setBold(true);
                    m0.setColor(ChatColor.YELLOW);
                    TextComponent m1 = new TextComponent("You need to have this plot protected before building here.");
                    m1.setBold(false);
                    m1.setColor(ChatColor.GRAY);
                    m0.addExtra(m1);
                    event.getPlayer().spigot().sendMessage(m0);
                } else if (!sql.owner.equals(event.getPlayer().getName())) {
                    event.setCancelled(true);

                    TextComponent m0 = new TextComponent("[ DPlot ] ");
                    m0.setBold(true);
                    m0.setColor(ChatColor.YELLOW);
                    TextComponent m1 = new TextComponent(
                            "This plot is owned by another player. You are not allowed to place blocks here.");
                    m1.setBold(false);
                    m1.setColor(ChatColor.GRAY);
                    m0.addExtra(m1);
                    event.getPlayer().spigot().sendMessage(m0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
