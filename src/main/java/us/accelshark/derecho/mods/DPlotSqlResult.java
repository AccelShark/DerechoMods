package us.accelshark.derecho.mods;

import java.util.List;

public class DPlotSqlResult {
    /** This enum entry is used when our plot is in cache but nobody owns it. */
    public static final int NONE_RESULT = -2;

    /** This enum entry is used when this plot is NOT cached. */
    public static final int NULL_RESULT = -1;

    boolean isProtected;
    int idx = NULL_RESULT;
    int plotX;
    int plotY;
    String owner;
    List<String> editors;
}
