package us.accelshark.derecho.mods;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public class Derecho extends JavaPlugin {
    private static DSqlConnection sqlConnection = null;
    private static Logger myLogger = null;
    private File configurationFile = null;
    private FileConfiguration configurationYaml = null;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new DPlotBlockListener(), this);
        getServer().getPluginManager().registerEvents(new DPlotChunkListener(), this);
        
        myLogger = getLogger();
        
        configurationFile = new File(this.getDataFolder(), "derecho.yml");
        configurationYaml = YamlConfiguration.loadConfiguration(configurationFile);
        try {
            sqlConnection = new DSqlConnection(configurationYaml.getString("sql.uri"));   
        } catch (Exception e) {
            Derecho.staticLogger().severe("Could not connect to SQL server");
            e.printStackTrace();
        }
        Derecho.staticLogger().info("Enabled");
    }

    @Override
    public void onDisable() {
        sqlConnection.close();
        Derecho.staticLogger().info("Disabled");
        myLogger = null;
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new DPlotChunkGenerator();
    }

    public static DSqlConnection getSqlConnection() {
        return sqlConnection;
    }

    public static Logger staticLogger() {
        return myLogger;
    }
}