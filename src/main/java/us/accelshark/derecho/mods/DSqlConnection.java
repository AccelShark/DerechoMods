package us.accelshark.derecho.mods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;

public class DSqlConnection {
    private static final int CACHE_SIZE = 256;

    private Connection connection = null;

    private HashMap<String, String> bannedItemsCacheMap = null;
    private DPlotSqlResult[] protectionsCache = new DPlotSqlResult[CACHE_SIZE];
    private int protectionsCacheCursor = 0;

    /**
     * Create a MySQL connection, usually used by the plugin application itself
     * 
     * @param where the URL of the MySQL database
     * @throws Exception
     */
    public DSqlConnection(String where) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager.getConnection(where);
            evictProtectionCache();
            updateBannedItems();
        } catch (ClassNotFoundException e) {
            Derecho.staticLogger().severe("Could not find MySQL JDBC");
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Checks the banned item map for the specified key
     * 
     * @param keyString the item's namespaced ID in string form
     * @return whether the item is banned or not
     */
    public boolean checkIfBannedItem(String keyString) {
        return bannedItemsCacheMap.containsKey(keyString);
    }

    /**
     * Returns the reason why the item is banned
     * 
     * @param keyString the item's namespaced ID in string form
     * @return the reason for the item ban, inside the MySQL database
     */
    public String getBannedItemReason(String keyString) {
        return bannedItemsCacheMap.get(keyString);
    }

    public void updateBannedItems() throws SQLException {
        Statement bannedItemsStatement = null;
        ResultSet bannedItemsResultSet = null;
        try {
            bannedItemsStatement = connection.createStatement();

            Derecho.staticLogger().info("Executing a greedy query for banned items...");
            bannedItemsResultSet = bannedItemsStatement.executeQuery("select * from derecho.banned_items");

            bannedItemsCacheMap = new HashMap<String, String>();
            while (bannedItemsResultSet.next()) {
                bannedItemsCacheMap.put(bannedItemsResultSet.getString("entry"),
                        bannedItemsResultSet.getString("reason"));
            }
        } catch (SQLException e) {
            Derecho.staticLogger().severe("DATABASE PROBLEM! Please stop the server and promptly fix it.");
            e.printStackTrace();
        } finally {
            if (bannedItemsResultSet != null) {
                bannedItemsResultSet.close();
            }
            if (bannedItemsStatement != null) {
                bannedItemsStatement.close();
            }
        }
    }

    public DPlotSqlResult getProtectionAt(int x, int y) throws SQLException {
        DPlotSqlResult result = new DPlotSqlResult();
        PreparedStatement statement = null;
        ResultSet sqlResult = null;
        result.isProtected = false;
        result.plotX = x;
        result.plotY = y;

        // Check the cache
        for (int i = 0; i < protectionsCache.length; i++) {
            DPlotSqlResult potentialResult = protectionsCache[i];
            if (potentialResult.idx == DPlotSqlResult.NULL_RESULT) {
                continue;
            }
            if (potentialResult.plotX == x && potentialResult.plotY == y) {
                result.idx = potentialResult.idx;
                result.isProtected = potentialResult.isProtected;
                result.owner = potentialResult.owner;
                result.editors = potentialResult.editors;

                // Derecho.staticLogger().info("Found cache entry for " + x + " " + y);
            }
        }

        if (result.idx == DPlotSqlResult.NULL_RESULT) {
            // Derecho.staticLogger().info("Missed cache entry for " + x + " " + y);
            try {
                statement = connection.prepareStatement(
                        "select * from derecho.plots_overworld where plotx = ? and ploty = ? limit 1");
                statement.setInt(1, x);
                statement.setInt(2, y);

                sqlResult = statement.executeQuery();

                if (sqlResult.next()) {
                    result.isProtected = true;
                    result.idx = sqlResult.getInt("idx");
                    result.owner = sqlResult.getString("owner");
                    String editors = sqlResult.getString("editors");
                    if (editors != null) {
                        result.editors = Arrays.asList(editors.split(","));
                    }
                }

                // Choose to cache here but specify nothingness OR specify all.
                if(result.idx == DPlotSqlResult.NULL_RESULT) {
                    protectionsCache[protectionsCacheCursor].idx = DPlotSqlResult.NONE_RESULT;
                } else {
                    protectionsCache[protectionsCacheCursor].idx = result.idx;
                }

                protectionsCache[protectionsCacheCursor].plotX = result.plotX;
                protectionsCache[protectionsCacheCursor].plotY = result.plotY;
                protectionsCache[protectionsCacheCursor].isProtected = result.isProtected;
                protectionsCache[protectionsCacheCursor].owner = result.owner;
                protectionsCache[protectionsCacheCursor].editors = result.editors;

                protectionsCacheCursor++;
                protectionsCacheCursor %= CACHE_SIZE;

            } catch (SQLException e) {
                Derecho.staticLogger().severe("DATABASE PROBLEM! Please stop the server and promptly fix it.");
                e.printStackTrace();
            } finally {
                if (sqlResult != null) {
                    sqlResult.close();
                }
                if (statement != null) {
                    statement.close();
                }
            }
        }
        return result;
    }

    public void evictProtectionCache() {
        Derecho.staticLogger().warning("Evicting protections cache...");
        for (int i = 0; i < protectionsCache.length; i++) {
            protectionsCache[i] = new DPlotSqlResult();
        }
        protectionsCacheCursor = 0;
    }

    /**
     * Closes the MySQL connection
     */
    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
        }
    }
}
